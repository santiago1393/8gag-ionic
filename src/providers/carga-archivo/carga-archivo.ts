
import { Injectable } from '@angular/core';

import { AngularFireDatabase } from "angularfire2/database";
import * as firebase from "firebase"
import { ToastController } from "ionic-angular";

import 'rxjs/add/operator/map';

/*
  Generated class for the CargaArchivoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CargaArchivoProvider {

  imagenes: ArchivoSubir[] = [];
  lastkey:string = null;

  constructor(public toastCtrl:ToastController,
              public afd:AngularFireDatabase) {
    console.log('Hello CargaArchivoProvider Provider');
    this.load_last_key().subscribe( () => {
      this.cargar_imagenes();
    });
  }

  cargar_imagenes(){

    let promesa = new Promise( (resolve, reject) => {
      this.afd.list('/posts',
       ref => ref.limitToLast(3)
                  .orderByKey()
                  .endAt(this.lastkey))
                  .valueChanges()
                  .subscribe((posts:any) => {
                    posts.pop();
                    if(posts.length == 0){
                      // Borro la ultima entrada para evitar que la duplique
                    
                      console.log("no hay mas registros");
                      resolve(false);
                      return;
                    }
                      this.lastkey = posts[0].key;

                      for (let index = posts.length-1; index >= 0; index--) {
                        let post = posts[index];
                        this.imagenes.push(post);
                      }
                      resolve(true);
                    
                  });
    });
    return promesa;
  }

  private load_last_key(){
    return this.afd.list('/posts', ref =>  ref.orderByKey().limitToLast(1))
            .valueChanges()
            .map( (post:any) => {
              console.log(post);
              this.lastkey = post[0].key;
              this.imagenes.push(post[0]);
            })
  }

  cargar_imagen_firebase( archivo:ArchivoSubir ){
    let promesa = new Promise((resolve, reject) => {
      this.mostrar_toast("...Cargando");
      let storeRef = firebase.storage().ref();
      let nombreArchivo:string = new Date().valueOf().toString();
      let uploadTask:firebase.storage.UploadTask = 
        storeRef.child(`img/${nombreArchivo}`).putString(archivo.img, 'base64',  {contentType:'image/jpg'});
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, 
          () => {}, // saber cuantos megas se han subido 
          (error) => {
            // manejo de error
            console.log("Error en la carga: ");
            console.log(JSON.stringify(error));
            this.mostrar_toast(JSON.stringify(error));
            reject();
          },
          () => {
            // todo bien!!
            console.log("Archivo subido");
            this.mostrar_toast("Imagen Cargada OK");

            uploadTask.snapshot.ref.getDownloadURL().then((downloadUrl) => {
              console.log("File available at: " + downloadUrl);
              let url = downloadUrl;
              this.crear_post(archivo.titulo, url, nombreArchivo );
              resolve();
            })       
          })
    });

    return promesa;
  }

  private crear_post(titulo:string, url:string, nombreArchivo:string){
    let post : ArchivoSubir = {
      titulo:titulo,
      img:url,
      key:nombreArchivo
    };

    console.log(JSON.stringify(post));

    //this.afd.list('/posts').push(post);
    this.afd.object(`/posts/${nombreArchivo}`).update(post);
    this.imagenes.push(post);

  }

  mostrar_toast(texto:string){
    let toast = this.toastCtrl.create({message:texto, duration:3000});
    toast.present();
  }

  

}


interface ArchivoSubir{
  titulo:string;
  img:string;
  key?:string;
}