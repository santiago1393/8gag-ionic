import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { Geolocation, Coordinates } from '@ionic-native/geolocation';


import { CargaArchivoProvider } from '../../providers/carga-archivo/carga-archivo';
/**
 * Generated class for the SubirPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subir',
  templateUrl: 'subir.html',
})
export class SubirPage {

  titulo:string = "";
  imagenPreview:string = "";
  imagen64:string;
  coords:Coordinates;

  constructor(private _cargarFb:CargaArchivoProvider,
              private geolocation: Geolocation,
              private imagePicker: ImagePicker,
              public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl:ViewController,
              public camera:Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubirPage');
    this.get_position().then(()=>{
      console.log("Latitude: " + this.coords.latitude);
      console.log("Longitude: " + this.coords.longitude);
      console.log("Altitude: " + this.coords.altitude);
      console.log("Accuracy: " + this.coords.accuracy);
    });
    
  }

  cerrar_modal(){
    this.viewCtrl.dismiss();
  }

  get_position(){
    let promise = new Promise((resolve,reject)=>{
      let options:PositionOptions = { enableHighAccuracy:true};
      this.geolocation.getCurrentPosition(options).then((resp) => {
        this.coords = resp.coords;
        resolve();
       }).catch((error) => {
         console.log('Error getting location', error);
         reject();
       });
    });

    return promise; 
     
  }

  mostrar_camara(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     this.imagenPreview = 'data:image/jpg;base64,' + imageData;
     this.imagen64 = imageData;
    }, (err) => {
     // Handle error
     console.log("Error en Camara: " + JSON.stringify(err));
    });
  }

  select_photo(){    

    let opciones:ImagePickerOptions = {
      quality:70,
      outputType:1,
      maximumImagesCount:1
    }
    this.imagePicker.getPictures(opciones).then((results) => {
      for (var i = 0; i < results.length; i++) {
          this.imagenPreview = 'data:image/jpg;base64,' +  results[i];
          this.imagen64 = results[i];
         // console.log('Image URI: ' + results[i]);
      }
    }, (err) => {
      console.log("Error en seleccionar: " + JSON.stringify(err));
     });
  }

  crear_post(){
    let archivo = {
      img:this.imagen64,
      titulo:this.titulo
    }
    this._cargarFb.cargar_imagen_firebase(archivo).then( () =>{
      this.cerrar_modal();
    });
    
  }
}
