import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { SubirPage } from "../subir/subir";

//import { AngularFireDatabase } from '@angular/fire/database';
//import { Observable } from 'rxjs/Observable';

import { CargaArchivoProvider } from '../../providers/carga-archivo/carga-archivo';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  //posts: Observable<any[]>;
  morePics:boolean = true;

  constructor(private _cap:CargaArchivoProvider,
              public navCtrl: NavController, 
              public modalCtrl:ModalController,
              private socialSharing: SocialSharing
              /*afDB: AngularFireDatabase*/) {
    //this.posts = afDB.list('posts').valueChanges();
  }

  mostrar_modal(){
    let modal = this.modalCtrl.create(SubirPage);
    modal.present();
  }

  
  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    this._cap.cargar_imagenes().then( (more:boolean) => {
      console.log(more);
      this.morePics = more;        
      infiniteScroll.complete();
    });
  }

  compartir(titulo:string, imagen:string ){
    
    this.socialSharing.shareViaInstagram(titulo,imagen).then(() => {
      // Success!
      console.log("OK");
    }).catch((error) => {
      console.log(error);
    });
  }

}
